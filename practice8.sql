﻿USE practice
SELECT
    p.item_id,
    p.item_name,
    p.item_price,
    p1.category_name
FROM
    item p
LEFT OUTER JOIN
    item_category p1
ON
    p.category_id = p1.category_id
ORDER BY
    p.item_id ASC
