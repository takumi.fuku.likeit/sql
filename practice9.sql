﻿USE practice
SELECT
    p1.category_name,
    SUM(p.item_price) AS total_price
FROM
    item_category p1
INNER JOIN
    item p
ON
    p.category_id = p1.category_id
GROUP BY
    category_name
