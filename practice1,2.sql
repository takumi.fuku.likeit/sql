﻿CREATE DATABASE practice DEFAULT CHARACTER 
SET
    utf8; 

USE practice CREATE TABLE item_category( 
    category_id int PRIMARY KEY AUTO_INCREMENT
    , category_name varchar (256) NOT NULL
); 

CREATE TABLE item( 
    item_id int PRIMARY KEY AUTO_INCREMENT
    , item_name VARCHAR (256) NOT NULL
    , item_price int NOT NULL
    , category_id int
);

